<?php

/**
 * Form callback for admin/structure/menu/manage/%menu/copy.
 */
function domain_menus_copy_menu_form($form, &$form_state, $menu) {
  $form_state['menu'] = $menu;

  $languages = language_list('language');
  $languages = array_map(function($language) {
    return $language->name;
  }, $languages);
  $languages = array(LANGUAGE_NONE => t('Language neutral')) + $languages;
  $form['languages'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Languages'),
    '#options' => $languages,
    '#default_value' => array_keys($languages),
    '#required' => TRUE,
    '#description' => t('Will only copy menu links with these languages.'),
    '#access' => module_exists('locale'),
  );

  $menus = array_map(function($menu) {
    return $menu['title'];
  }, menu_load_all());
  natcasesort($menus);
  unset($menus[$menu['menu_name']]);
  $form['target_menu'] = array(
    '#type' => 'select',
    '#title' => t('Target menu'),
    '#options' => $menus,
    '#required' => TRUE,
    '#description' => t('The menu the links will be copied to. Duplicates will not be filtered.'),
  );

  $form['delete_links'] = array(
    '#type' => 'checkbox',
    '#title' => t("Delete target menu's links first"),
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Copy'),
  );

  $form['#submit'][] = 'domain_menus_copy_menu_submit';

  return $form;
}

/**
 * Submit handler for domain_menus_copy_menu_form().
 */
function domain_menus_copy_menu_submit($form, &$form_state) {
  $menu = $form_state['menu'];
  $values = &$form_state['values'];

  $languages = array_values(array_filter($values['languages']));
  $target_menu = $values['target_menu'];

  // Delete existing menu links.
  if ($values['delete_links']) {
    db_delete('menu_links')
      ->condition('menu_name', $target_menu)
      ->execute()
    ;
  }

  // Do the copying.
  $mlids = _domain_menus_copy_menu_links($menu['menu_name'], $target_menu, $languages);

  $form_state['redirect'] = 'admin/structure/menu/manage/' . $target_menu;
}

/**
 * Form callback for admin/structure/domain/menus.
 */
function domain_menus_form($form, &$form_state) {
  // Make sure this form doesn't end up as "domain-sensitive".
  $forms_visibility = &drupal_static('domain_settings_add_element', array());
  $forms_visibility[__FUNCTION__] = FALSE;

  $missing_menus = domain_menus_missing();
  $form['status'] = array(
    '#type' => 'fieldset',
    '#title' => t('Status'),
  );
  if ($missing_menus) {
    $form['status']['create'] = array(
      '#type' => 'submit',
      '#value' => t('Create menus'),
      '#prefix' => t('<span class="error">@num menus are missing.</span> You can bulk-auto-create them:', array('@num' => count($missing_menus))) . ' ',
      '#suffix' => '<br>' . t('Missing menus: %menus', array('%menus' => implode(', ', array_keys($missing_menus)))),
      '#submit' => array('domain_menus_bulk_auto_create_submit'),
    );
  }
  else {
    $form['status']['#collapsible'] = TRUE;
    $form['status']['#collapsed'] = TRUE;
    $form['status']['exist'] = array(
      '#type' => 'item',
      '#title' => t('Status'),
      '#title_display' => 'invisible',
      '#markup' => t('All configured menus seem to exist.'),
    );
  }

  $menus = domain_menus();
  $defaults = array_keys(_domain_menus_default());
  $menus[] = array_combine($defaults, array_fill(0, count($defaults), ''));

  $domain = domain_get_domain();
  $options = array('sanitize' => TRUE);

  $form['domain_menus'] = array('#tree' => TRUE);
  foreach ($menus as $menu) {
    $title = token_replace($menu['title'], array('domain' => $domain), $options);
    $description = token_replace($menu['description'], array('domain' => $domain), $options);

    $menu_form = array(
      '#type' => 'fieldset',
      '#title' => $menu['title'] ?: t('New menu'),
      'prefix' => array(
        '#type' => 'textfield',
        '#title' => t('Prefix'),
        '#default_value' => $menu['prefix'],
        '#maxlength' => 28, // menu_custom.menu_name size is 32.
        '#description' => t('Make sure that this prefix + "-" + [domain id] is not longer than the allowed menu machine name. No tokens allowed.'),
      ),
      'title' => array(
        '#type' => 'textfield',
        '#title' => t('Title'),
        '#default_value' => $menu['title'],
        '#description' => '<em>' . $title . '</em>',
      ),
      'description' => array(
        '#type' => 'textfield',
        '#title' => t('Description'),
        '#default_value' => $menu['description'],
        '#description' => '<em>' . $description . '</em>',
      ),
      'variable' => array(
        '#type' => 'textfield',
        '#title' => t('Variable'),
        '#default_value' => $menu['variable'],
        '#description' => t('This domain variable (domain conf) will be set to the new menu name.'),
        '#access' => module_exists('domain_conf'),
      ),
      'home_link' => array(
        '#type' => 'textfield',
        '#title' => t('Home link'),
        '#default_value' => $menu['home_link'],
      ),
    );
    $form['domain_menus'][] = $menu_form;
  }

  $form['#submit'][] = 'domain_menus_form_submit';

  return system_settings_form($form);
}

/**
 * Submit handler for domain_menus_form().
 */
function domain_menus_form_submit($form, &$form_state) {
  // Filter domain menus.
  $form_state['values']['domain_menus'] = _domain_menus_filter($form_state['values']['domain_menus']);
}

/**
 * Submit handler for domain_menus_form().
 */
function domain_menus_bulk_auto_create_submit($form, &$form_state) {
  $missing_menus = domain_menus_missing();
  foreach ($missing_menus as $menu) {
    _domain_menu_create($menu['domain'], $menu['menu']);
  }

  drupal_set_message(t('Created @num menus.', array('@num' => count($missing_menus))));
}
